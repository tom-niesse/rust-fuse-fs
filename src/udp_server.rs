use std::thread;
use std::net::UdpSocket;
use std::sync::{Arc, Mutex};
use md5;
use std::time::Duration;
use fountain_codes::{Encoder, Decoder};

pub struct UdpServer {
    pub host: String,
    pub buffer: Arc<Mutex<Vec<Vec<u8>>>>,
    pub last_packet_id: Arc<Mutex<u8>>
}

impl UdpServer {
    pub fn new(host: String) -> Self {
        Self {
            host,
            buffer: Arc::new(Mutex::new(vec![])),
            last_packet_id: Arc::new(Mutex::new(u8::MAX))
        }
    }
    pub fn run(&mut self) {
        let mut socket = UdpSocket::bind(&self.host);
        loop {
            let mut network_buffer: Vec<u8> = vec![0; 65500];
            let mut packet_data: Vec<u8> = vec![];
            packet_data.push(0);
            let mut amount_of_chunks = 0;
            loop {
                let (size, src) = socket.as_ref().expect("Failed to bind socket").recv_from(&mut network_buffer).unwrap();
                let mut data: Vec<u8> = vec![];
                data.extend(&network_buffer[..size]);
                let packet_id = data[0];
                packet_data[0] = packet_id;
                let last_chunk = data[1] > 0;
                packet_data.extend(&data[2..]);
                if last_chunk {
                    break;
                }
                amount_of_chunks+=1;
            }
            let packet_id = packet_data[0];
            let mut data: Vec<u8> = vec![];
            data.extend(&packet_data[1..]);
            if packet_id != *self.last_packet_id.lock().unwrap() {
                *self.last_packet_id.lock().unwrap() = packet_id;
                self.buffer.lock().unwrap().push(data);
            }
        }
    }
    pub fn buffer(&self) -> Arc<Mutex<Vec<Vec<u8>>>> {
        self.buffer.clone()
    }
}

