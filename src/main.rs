use std::thread;
use std::time::Duration;
use std::thread::JoinHandle;
use std::ffi::OsStr;
use std::env;
use std::process::Command;
use std::fs;

mod diode_fs;
use diode_fs::{DiodeFS};
mod diode_fs_injector;
use diode_fs_injector::{DiodeFSInjector, FilesystemCommit};
mod udp_client;
use udp_client::UdpClient;
mod udp_server;
use udp_server::UdpServer;

const CHUNK_SIZE: usize = 64000;
const REDUNDANCY: usize = 1;

fn main() {
    let args: Vec<String> = env::args().collect();
    let mode: String = args[1].clone();
    let host: String = args[2].clone();
    let mount_point: String;
    if mode == "send" {
        mount_point = "/send".to_string();
    } else {
        mount_point = "/receive".to_string();
    }
    // Unmount previous (or dangling) FUSE mounts
    Command::new("fusermount").args(["-u", &mount_point]).output().expect("failed to execute process");
    // Remove all files in the FUSE mounts directory
    fs::remove_dir_all(mount_point.clone()).unwrap();
    fs::create_dir(mount_point.clone()).unwrap();

    let mut threads: Vec<JoinHandle<()>> = vec![];
    let fs = DiodeFS::new();
    let filesystem_access = fs.get_filesystem_items();
    let filesystem_commit_access = fs.get_filesystem_commits();

    // Make a thread that does filesystem stuff
    let fuse_mount_point = mount_point.clone().to_string();
    threads.push(thread::spawn(move||{
        let mountpoint = fuse_mount_point;
        let options = [
            OsStr::new("-o"), OsStr::new("atomic_o_trunc"),
            OsStr::new("-o"), OsStr::new("allow_root"),
            // OsStr::new("-o"), OsStr::new("allow_other"),
        ];
        let _ = fuse::mount(fs, mountpoint, &options);
    }));

    let ftp_mount_point = mount_point.clone();
    if mode == "send" {
        // Make a thread that UDP send's every commit and then deletes it from the commit list
        threads.push(thread::spawn(move||{
            let mut udp_client = UdpClient::new();
            loop {
                while !filesystem_commit_access.lock().unwrap().is_empty() {
                    let mut commits = filesystem_commit_access.lock().unwrap();
                    udp_client.send(host.to_string(), commits[0].to_bytes());
                    commits.remove(0);
                }
                thread::sleep(Duration::from_millis(1));
            }
        }));
    } else {
        // Make a thread that receives data via UDP
        let mut udp_server = UdpServer::new(host.to_string());
        let mut udp_server_buffer = udp_server.buffer();
        threads.push(thread::spawn(move||{
            udp_server.run();
        }));
        // Make a thread that injects files directly into the filesystem
        let injector_commit_access = filesystem_commit_access.clone();
        threads.push(thread::spawn(move||{
            let mut injector = DiodeFSInjector {
                filesystem_items: filesystem_access,
                filesystem_commits: injector_commit_access
            };
            loop {
                while !udp_server_buffer.lock().unwrap().is_empty() {
                    let data: Vec<u8> = udp_server_buffer.lock().unwrap()[0].clone();
                    injector.handle_commit(FilesystemCommit::from_bytes(data));
                    udp_server_buffer.lock().unwrap().remove(0);
                }
                thread::sleep(Duration::from_millis(10));
            }
        }));
    }

    for thread in threads {
        let _ = thread.join();
    }
}