use std::net::UdpSocket;
use std::time::Duration;
use std::thread;
use md5;

pub struct UdpClient {
    packet_id: u8,
    chunk_id: u8
}

impl UdpClient {
    pub fn new() -> Self {
        Self {
            packet_id: 0,
            chunk_id: 0
        }
    }
    pub fn send(&mut self, to: String, data: Vec<u8>) {
        let socket = UdpSocket::bind("0.0.0.0:0").expect("Failed to bind host socket.");
        socket.set_broadcast(true).expect("Failed to do set_broadcast=true.");
        let chunks = data.chunks(65500-2);
        let chunks_len = chunks.len();
        let mut out: Vec<Vec<u8>> = vec![];
        for (chunk_id, chunk) in chunks.enumerate() {
            let mut last_chunk: bool = false;
            if chunk_id+1 == chunks_len {
                last_chunk = true;
            }
            let mut packet: Vec<u8> = vec![];
            packet.push(self.packet_id);
            packet.push(last_chunk as u8);
            packet.extend(chunk);
            out.push(packet);
        }
        for chunk in out {
            let _ = socket.send_to(&chunk, to.clone());
        }
        if self.packet_id == u8::MAX {
            self.packet_id = 0;
        } else {
            self.packet_id+=1;
        }
    }
}
