.PHONY: send receive

all:
	clear
	-sudo fusermount -u /home/tom/projects/fuse_test/mounts/send
	-sudo rm -rf /home/tom/projects/fuse_test/mounts/send/*
	cargo build
	cp target/debug/fuse_test docker/send
	cp target/debug/fuse_test docker/receive

run: all
	cd docker && docker-compose up --build && cd ..
